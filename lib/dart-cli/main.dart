import 'dart:collection';
import 'dart:io';
import 'dart:math';
import 'package:intl/intl.dart';
import 'package:dart_cli_cards/dart-cli/card.dart';

// Class used as an enum holding int values
class DiscountEnum {
  final int _value;

  const DiscountEnum._(this._value);

  int toInt() {
    return _value;
  }

  static const DiscountEnum FIVE = DiscountEnum._(5);
  static const DiscountEnum TEN = DiscountEnum._(10);
  static const DiscountEnum TWENTY = DiscountEnum._(20);
  static const DiscountEnum THIRTY = DiscountEnum._(30);
}

class CategoryEnum {
  final int _value;

  const CategoryEnum._(this._value);

  int toInt() {
    return _value;
  }

  static const CategoryEnum COSMETICS = CategoryEnum._(1);
  static const CategoryEnum BOOKS = CategoryEnum._(2);
  static const CategoryEnum ACCESSORIES = CategoryEnum._(3);
  static const CategoryEnum SERVICES = CategoryEnum._(4);
}

main() {
  var listOfCards = [];

  // Add 2 test cards
  listOfCards.add(testCard());
  listOfCards.add(testCard1());
  // 12345678901234
  // 30201405191234

  final message = '\nThis is the main menu\n'
      'If you want to read all cards, write 1 \n'
      'If you want to read a card details, write 2\n'
      'If you want to search for a card by a number, write 3\n'
      'If you want to create a new card, write 4 \n'
      'If you want to update a card by its number, write 5 \n'
      'If you want to delete a card, write 6 \n'
      'If you want to exit, write 7 \n'
      'Enter number here:';
  stdout.writeln(message);
  var enteredValue = int.parse(stdin.readLineSync());
  var card;

  while (true) {
    switch (enteredValue) {
      case 1:
        for (Card card in listOfCards) {
          stdout.writeln('Card code: ' + card.cardCode);
          stdout.writeln('Customer name: ' + card.customerFullName);
        }
        break;
      case 2:
        checkIfCardExists(listOfCards);
        break;
      case 3:
        searchForACardByItsNumber(listOfCards);
        break;
      case 4:
        card = createACard();
        listOfCards.add(card);
        break;
      case 5:
        updateACardByItsNumber(listOfCards);
        break;
      case 6:
        if (listOfCards.isEmpty) {
          stdout.writeln('The list is empty, exiting to the main menu.');
          stdout.writeln(message);
          continue;
        }
        stdout.writeln(
            "Enter the code number of the card that you want to remove");
        var enteredCardCodeNum = stdin.readLineSync();
        var currCard;
        for (Card card in listOfCards) {
          if (enteredCardCodeNum == card.cardCode) {
            currCard = card;
            break;
          }
        }
        if (currCard == null) {
          stdout.writeln('Card was not found, exiting to the main menu.');
          stdout.writeln(message);
          continue;
        } else {
          stdout.writeln('The card was found. '
              'If you want to delete the card write yes, else write no \nEnter the word:');
          var answer = stdin.readLineSync();
          if (answer == 'yes') {
            listOfCards.remove(currCard);
            print('The card was deleted');
          } else {
            stdout
                .writeln('The card was not delete, exiting to the main menu.');
            stdout.writeln(message);
            continue;
          }
        }
        break;
      case 7:
        stdout.writeln('You exited the program');
        return;
        break;
      default:
        stdout.writeln(message);
        enteredValue = int.parse(stdin.readLineSync());
        continue;
    }

    print('');
    stdout.writeln(message);
    enteredValue = int.parse(stdin.readLineSync());
  }
}

void updateACardByItsNumber(List listOfCards) {
  stdout.writeln("Enter the number of the card that you want to update:");
  var cardNumber = stdin.readLineSync();
  Card cardForUpdate;
  Card card;
  for (card in listOfCards) {
    if (card.cardCode == cardNumber) {
      cardForUpdate = card;
    }
  }
  stdout.writeln("Enter new full name:");
  var fullName = stdin.readLineSync();
  stdout.writeln("Enter new city name:");
  var cityName = stdin.readLineSync();

  // Get card number details
  var category = getACategory();
  var accumulation = getAccumulation();
  var discount = getDiscount();
  var expirationDate = getExpirationDate();
  var uniqueIdentifier = getUniqueIdFrom0000To9999();
  var cardCodeValue =
      category + accumulation + discount + expirationDate + uniqueIdentifier;

  cardForUpdate.customerFullName = fullName;
  cardForUpdate.customerCity = cityName;
  cardForUpdate.cardCode = cardCodeValue;

  stdout.writeln("Updated card details:");
  stdout.writeln('Customer name: ${cardForUpdate.customerFullName}');
  stdout.writeln('Customer city: ${cardForUpdate.customerCity}');
  stdout.writeln('Card code: ${cardForUpdate.cardCode}');
}

void searchForACardByItsNumber(List listOfCards) {
  stdout.writeln(
      'Enter at least 3 digits that are a part of a card\'s code number:');
  var searchedDigitsSequence = stdin.readLineSync();
  var isContained = false;
  const minLength = 3;
  const maxLength = 14;
  if (searchedDigitsSequence.length >= minLength &&
      searchedDigitsSequence.length <= maxLength) {
    for (Card card in listOfCards) {
      var cardNum = card.cardCode;
      isContained =
          cardNum.contains(new RegExp('[0-9]$searchedDigitsSequence[0-9]*'));
      if (isContained) {
        stdout.writeln('The card number is contained, it is: $cardNum');
        break;
      }
    }
  } else {
    stdout.writeln('The input was not in the correct bounds');
    return;
  }
  if (!isContained) {
    stdout.writeln('The card with this card number was not found');
  }
}

void checkIfCardExists(List listOfCards) {
  stdout.writeln('Enter the code of the card:');
  var cardCodeFromStdin = stdin.readLineSync();
  var foundCard = (listOfCards.singleWhere(
      (card) => card.cardCode == cardCodeFromStdin,
      orElse: () => null));
  if (foundCard != null) {
    stdout.writeln('The card was found, its details are: \n');
    stdout.writeln(foundCard.customerFullName);
    stdout.writeln(foundCard.customerCity);
    stdout.writeln(foundCard.cardCode);
  } else {
    stdout.writeln('The card was not found.');
  }
}

Card createACard() {
  stdout.writeln("Enter full name:");
  var fullName = stdin.readLineSync();
  stdout.writeln("Enter your city name:");
  var cityName = stdin.readLineSync();

  // Get card number details
  var category = getACategory();
  var accumulation = getAccumulation();
  var discount = getDiscount();
  var expirationDate = getExpirationDate();
  var uniqueIdentifier = getUniqueIdFrom0000To9999();
  var cardCodeValue =
      category + accumulation + discount + expirationDate + uniqueIdentifier;

  var card = Card(fullName, cityName, cardCodeValue);
  return card;
}

var counter = -1;

String getUniqueIdFrom0000To9999() {
  if (counter < 9999) {
    counter += 1;
  } else {
    // Starts again from 0000
    counter = 0;
  }
  return counter.toString().padLeft(4, '0');
}

String getExpirationDate() {
  DateTime now = DateTime.now().add(Duration(days: 365));
  DateFormat formatter = DateFormat('ddMMyy');
  var expirationDate = formatter.format(now);
  return expirationDate;
}

String getDiscount() {
  stdout.writeln("Choose a discount number from these options: 5,10,20,30"
      "\nEnter the chosen number:");
  var chosenDiscount = int.parse(stdin.readLineSync());
  while (true) {
    if (chosenDiscount == DiscountEnum.FIVE.toInt() ||
        chosenDiscount == DiscountEnum.TEN.toInt() ||
        chosenDiscount == DiscountEnum.TWENTY.toInt() ||
        chosenDiscount == DiscountEnum.THIRTY.toInt()) {
      break;
    } else {
      stdout.writeln("Wrong input. \nEnter the chosen number again:");
      chosenDiscount = int.parse(stdin.readLineSync());
      continue;
    }
  }
  return chosenDiscount.toString();
}

String getAccumulation() {
  stdout.writeln("If you want accumulation write yes, else write no "
      "\nEnter the chosen word :");
  var accumulation = stdin.readLineSync();
  while (true) {
    if (accumulation == 'yes') {
      return '1';
    } else if (accumulation == 'no') {
      return '0';
    } else {
      stdout.writeln('Wrong input, try again \nEnter the chosen word :');
      accumulation = stdin.readLineSync();
      continue;
    }
  }
}

String getACategory() {
  stdout.writeln('Choose a category: 1 – cosmetics, 2 – books,'
      ' 3 – accessories, 4 – services \nEnter the chosen number :');
  var input = int.parse(stdin.readLineSync());
  var category;
  while (true) {
    if (input == CategoryEnum.COSMETICS.toInt()) {
      category = 'cosmetics';
    } else if (input == CategoryEnum.BOOKS.toInt()) {
      category = 'books';
    } else if (input == CategoryEnum.ACCESSORIES.toInt()) {
      category = 'accessories';
    } else if (input == CategoryEnum.SERVICES.toInt()) {
      category = 'services';
    } else {
      stdout.writeln('Wrong number try again: \n Enter a number:');
      input = int.parse(stdin.readLineSync());
      continue;
    }
    break;
  }

  return input.toString();
}

void createACardInAList() {
  var listOfCard = [];

  var customerFullName = 'Stanislav Petkov';
  var city = 'Sofia';
  var cardNumber = '30201405191234';
  var card = Card(customerFullName, city, cardNumber);

  var customerFullName1 = 'Stanislav1 Petkov1';
  var city1 = 'Sofia1';
  var cardNumber1 = '111111111111';
  var card1 = Card(customerFullName1, city1, cardNumber1);

  listOfCard.add(card);
  listOfCard.add(card1);

  print('');
}

Card testCard() {
  var customerFullName = 'Stanislav Petkov';
  var city = 'Sofia';
  var cardNumber = '30201405191234';
  var card = Card(customerFullName, city, cardNumber);
  return card;
}

Card testCard1() {
  var customerFullName = 'StanislavDemo PetkovDemo';
  var city = 'Sofia';
  var cardNumber = '12345678901234';
  var card = Card(customerFullName, city, cardNumber);
  return card;
}
